package com.testes.automatizados.integracao;

import com.testes.automatizados.Controller.ClienteController;
import com.testes.automatizados.DTO.ClienteDTO;
import com.testes.automatizados.Service.ClienteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.TestPropertySources;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.net.http.HttpRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

//Comentários importantes em Service/ClienteServiceTest
@ExtendWith(SpringExtension.class)

// Necessário para realizar a injeção de dependências
@SpringBootTest

/* Esta classe realiza testes com operação em banco de dados.
** A clausula a seguir define um arquivo de configuração diferente para
** executar os testes desta classe. Caso nenhum arquivo de configuração
** seja indicado, será usado o arquivo padrão
*/
@TestPropertySource(locations = "/test.properties")
public class ClienteControllerTest {

    @Autowired
    private ClienteController clienteController;

    @BeforeEach
    public void setUp(){
    }

    @Test
    public void testaInsercaoCliente(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate dataNasc = LocalDate.parse("01/01/2000", formatter);
        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setCpf("10248754896");
        clienteDTO.setNome("Rodrigo Faro");
        clienteDTO.setDataNascimento(dataNasc);

        ResponseEntity<Object> respostaObtida = clienteController.cadastrarCliente(clienteDTO);
        ResponseEntity<Object> respostaEsperada = new ResponseEntity<Object>(clienteDTO, HttpStatus.CREATED);

        Assertions.assertEquals(respostaEsperada.getStatusCode(), respostaObtida.getStatusCode());
        Assertions.assertEquals(respostaEsperada.getBody().toString(), respostaObtida.getBody().toString());

    }

}
