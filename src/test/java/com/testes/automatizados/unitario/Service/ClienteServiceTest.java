package com.testes.automatizados.unitario.Service;

import com.testes.automatizados.DTO.ClienteDTO;
import com.testes.automatizados.Entity.Cliente;
import com.testes.automatizados.Repository.ClienteRepository;
import com.testes.automatizados.Service.ClienteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
public class ClienteServiceTest {
    @InjectMocks //Define a classe que será testada e que receberá os objetos mocados
    private ClienteService clienteService;

    @Mock //cria um objeto mocado. Seu comportamento será definido pelo programador
    private ClienteRepository clienteRepository;

    //cria um objeto mocado. Seu comportamento pode ser monitorado, mas
    //se comportará normalmente, sem interferência do programador
    @Spy
    private ModelMapper modelMapper;

    //Define função que será executada antes de CADA teste
    //Para definir função que será executada apenas uma vez antes de todos os teste:
    //@BeforeAll
    @BeforeEach
    public void setUp(){
    }

    //Define um teste que será executado
    @Test
    public void testeCadastroCliente(){
        //Cria um objeto ClienteDTO para simular um cadastro no banco
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate dataNasc = LocalDate.parse("05/05/2005", formatter);
        ClienteDTO clienteDTO = new ClienteDTO("78945612396", "Adamastor", dataNasc);

        //configura o comportamento do objeto mocado
        Cliente cliente = new Cliente("78945612396", "Adamastor", dataNasc);
        Mockito.when(this.clienteRepository.save(any(Cliente.class))).thenReturn(cliente);

        //faz a chamada da função sendo testada
        ClienteDTO retornoDTO = clienteService.cadastrarCliente(clienteDTO);

        //verifica se o retorno foi como o esperado
        Assertions.assertEquals(clienteDTO.toString(), retornoDTO.toString());
    }

    @Test
    public void testeEdicaoCliente(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate dataNasc = LocalDate.parse("09/09/2009", formatter);
        ClienteDTO clienteDTO = new ClienteDTO("15948726351", "Fernando", dataNasc);

        Cliente cliente = new Cliente("15948726351", "Fernando", dataNasc);
        Mockito.when(clienteRepository.save(any(Cliente.class))).thenReturn(cliente);

        ClienteDTO retornoDTO = clienteService.alteraCliente(clienteDTO);

        Assertions.assertEquals(clienteDTO.toString(), retornoDTO.toString());
    }


}
