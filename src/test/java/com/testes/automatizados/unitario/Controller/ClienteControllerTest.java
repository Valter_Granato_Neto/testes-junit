package com.testes.automatizados.unitario.Controller;

import com.testes.automatizados.Controller.ClienteController;
import com.testes.automatizados.DTO.ClienteDTO;
import com.testes.automatizados.Service.ClienteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.mockito.ArgumentMatchers.any;

//Comentários importantes em Service/ClienteServiceTest
@ExtendWith(SpringExtension.class)
public class ClienteControllerTest {

    @InjectMocks
    private ClienteController clienteController;
    @Mock
    private ClienteService clienteService;

    @BeforeEach
    public void setUp(){
    }

    @Test
    public void testaCadastrarCliente(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate dataNasc = LocalDate.parse("01/01/2000", formatter);
        ClienteDTO clienteDTO = new ClienteDTO("12345678912", "Rodrigo", dataNasc);
        Mockito.when(clienteService.cadastrarCliente(any(ClienteDTO.class))).thenReturn(clienteDTO);

        ResponseEntity<Object> respostaResponseEntity = clienteController.cadastrarCliente(clienteDTO);

        System.out.println(respostaResponseEntity.toString());

        Assertions.assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(clienteDTO).toString(), respostaResponseEntity.toString());
    }
}
