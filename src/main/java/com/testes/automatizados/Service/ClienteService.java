package com.testes.automatizados.Service;

import com.testes.automatizados.DTO.ClienteDTO;
import com.testes.automatizados.Entity.Cliente;
import com.testes.automatizados.Repository.ClienteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository repository;
    @Autowired
    private ModelMapper modelMapper;

    public ClienteService(){}

    public ClienteRepository getRepository() {
        return repository;
    }
    public void setRepository(ClienteRepository repository) {
        this.repository = repository;
    }
    public ModelMapper getModelMapper() {
        return modelMapper;
    }
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Transactional
    public Page<ClienteDTO> retornaClientes(Pageable pageable){
        Page<Cliente> list = repository.findAll(pageable);
        return list.map(x -> modelMapper.map(x, ClienteDTO.class));
    }

    @Transactional
    public ClienteDTO cadastrarCliente(ClienteDTO clienteDTO){
        ClienteDTO retornoDto = new ClienteDTO();
        Cliente cliente = new Cliente();
        modelMapper.map(clienteDTO, cliente);
        modelMapper.map(repository.save(cliente), retornoDto);
        return retornoDto;
    }

    @Transactional
    public ClienteDTO alteraCliente(ClienteDTO clienteDTO){
        return modelMapper.map(repository.save(modelMapper.map(clienteDTO, Cliente.class)), ClienteDTO.class);
    }

    @Transactional
    public void removeCliente(String cpf){
        repository.deleteById(cpf);
    }

    @Transactional
    public boolean clienteExiste(ClienteDTO clienteDTO){
        return repository.existsById(clienteDTO.getCpf());
    }

}
