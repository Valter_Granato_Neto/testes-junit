package com.testes.automatizados.Controller;

import com.testes.automatizados.DTO.ClienteDTO;
import com.testes.automatizados.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    public void setClienteService(ClienteService clienteService){
        this.clienteService = clienteService;
    }

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    public ClienteController(){}

    @GetMapping
    public ResponseEntity<Page<ClienteDTO>> retornaClientes(Pageable pageable){
        return ResponseEntity.ok(clienteService.retornaClientes(pageable));
    }

    @PostMapping
    public ResponseEntity<Object> cadastrarCliente(@RequestBody ClienteDTO clienteDTO){
        if (clienteService.clienteExiste(clienteDTO)){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Este cliente já está cadastrado");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.cadastrarCliente(clienteDTO));
    }

    @PutMapping
    public ResponseEntity<Object> editaCliente(@RequestBody ClienteDTO clienteDTO){
        if (!clienteService.clienteExiste(clienteDTO)){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Este cliente não está cadastrado");
        }
        return ResponseEntity.status(HttpStatus.OK).body(clienteService.alteraCliente(clienteDTO));
    }

    @DeleteMapping("/{cpf}")
    public ResponseEntity<Object> removeCliente(@PathVariable(name = "cpf") String cpf){
        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setCpf(cpf);
        if (!clienteService.clienteExiste(clienteDTO)){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Este cliente não está cadastrado");
        }
        clienteService.removeCliente(cpf);
        return ResponseEntity.ok("");
    }


}
